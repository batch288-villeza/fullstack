// console.log("hi")

/*	Fetch keyword
		
		Syntax:
			fetch('url', {options})
	

	https://jsonplaceholder.typicode.com/posts
*/
// Show post
const showPosts = (posts) => {
	// console.log(typeof post)

	let entries = '';

	posts.forEach( post => {
		entries += `<div id="post-${post.id}">
						<h3 id="post-title-${post.id}">${post.title}</h3>
						<p id="post-body-${post.id}">${post.body}</p>

						<button onclick="editPost(${post.id})">Edit</button>
						<button onclick="deletePost(${post.id})">Delete</button>
					</div>`
	});

	document.querySelector('#div-post-entries').innerHTML = entries
}

// Edit Post
const editPost = (id) => {
	let title = document.querySelector(`#post-title-${id}`).innerHTML;
	let body = document.querySelector(`#post-body-${id}`).innerHTML;
	
	console.log(id);
	console.log(title);
	console.log(body);

	document.querySelector('#txt-edit-id').value = id
	document.querySelector('#txt-edit-title').value = title
	document.querySelector("#txt-edit-body").value = body

	//	removeAttribute('attribute_name')
	//		- this will the declared attribute from the element
	document.querySelector('#btn-submit-update').removeAttribute('disabled')
}


// Activity: Detele Post

const deletePost = (id) => {

	document.querySelector(`#post-${id}`).remove()

}



// get data from API
fetch("https://jsonplaceholder.typicode.com/posts")
.then(result => result.json())
.then(response => {
	console.log(response)
	showPosts(response)
});


// ADD / POST data on our API
document.querySelector('#form-add-post').addEventListener('submit',
	(event) => {
		// when the submit event is use, we must ad a paramaeter event to the function to capture the properties of oour event

		// to change the auto-reload of the submit method
		event.preventDefault();

		// POST method
		//	if we use the post request the fetch method will return the newly created document
		fetch("https://jsonplaceholder.typicode.com/posts", {
			method: 'POST',
			headers:{
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				ttile: document.querySelector("#txt-title").value,
				body: document.querySelector("#txt-body").value,
				userId: 1
			})
		})
		.then(response => response.json())
		.then(result => {
			console.log(result);

			alert('Post successfully added');

			document.querySelector("#txt-title").value = null;
			document.querySelector("#txt-body").value = null;
		});
	}
)


// Update Post
document.querySelector('#form-edit-post').addEventListener('submit', event => {
	event.preventDefault();

	let id = document.querySelector('#txt-edit-id').value

	fetch(`https://jsonplaceholder.typicode.com/posts/${id}`,{
		method: "PUT",
		headers: {'Content-Type': 'application/json'},
		body: JSON.stringify({
			title: document.querySelector('#txt-edit-title').value,
			body: document.querySelector('#txt-edit-body').value,
			userId: 1
		})
	})
	.then(response => response.json())
	.then(result => {
		console.log(result)
		
		//alert('The post was successfully updated!')

		document.querySelector(`#post-title-${id}`).innerHTML = document.querySelector('#txt-edit-title').value
		document.querySelector(`#post-body-${id}`).innerHTML = document.querySelector('#txt-edit-body').value


		document.querySelector('#txt-edit-id').value = null
		document.querySelector('#txt-edit-title').value = null
		document.querySelector("#txt-edit-body").value = null

		//	removeAttribute('attribute_name')
		//		- this will the declared attribute from the element
		document.querySelector('#btn-submit-update').setAttribute('disabled', true)
		})
})