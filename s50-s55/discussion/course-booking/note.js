/*
	Section I: ReactJS

		a. If you will create a ReactJS application:
			> npx create-react-app "project-name"

		b. ReactJS is strict with the application name, it should be in lower case
			> Example:
				- courseBooking - bad
				- course-booking - good
		
		c. Inside course-booking/src folder, delete the following:
			>App.test.js
			>index.css
			>logo.svg
			>reportWebvitals.js

		d. in App.js remove:
			import logo from './logo.svg';

		e. in index.js remove:
			import './index.css';
			import reportWebVitals from './reportWebVitals';

		e. go to package.json and delete:
			"eslintConfig": {
			  "extends": [
			    "react-app",
			    "react-app/jest"
			  ]
			},
		
		f. go to src folder and create the below folders:
			1. pages
			2. components


		f. Fragment (<></>)
			Syntax:
				<>
					reactjs components
				</>
			> it is use to contai



	
	Section II:
		> courses.js will server as our mock database in place ot our MongoDB ceacause we want to focus on ReactJS Concept.
		> Props
		> State
			- allows to create manage its own data and is meant to be used internally.
			a. hooks
				- https://legacy.reactjs.org/docs/hooks-intro.html
	
	Section III: Installation
		React Boostrap
			> npm install react-bootstrap
			> npm install bootstrap

			> Link:
				> https://www.npmjs.com/package/react-bootstrap
				> https://react-bootstrap.github.io/docs/getting-started/introduction

		React Router DOM
			> npm install react-router-dom

		Change Running port in Localhost:
			> go to package.json file
			> in script object, update the start field value to:
				"start": "set PORT=portnumber && react-scripts start"
		
		sweetalert2 package
			> npm install sweetalert2
			> https://sweetalert2.github.io/			

		For React Design
			> https://reactjsexample.com/tag/sidebar/#
			> https://www.shapedivider.app/
			> https://mui.com/material-ui/getting-started/installation/
				- npm install @mui/material @emotion/react @emotion/styled

		Change the output format of a number to currency:
			> https://www.freecodecamp.org/news/how-to-format-number-as-currency-in-javascript-one-line-of-code/
			> https://www.iban.com/currency-codes
			> https://saimana.com/list-of-country-locale-code/

		JavaScript Code
			> https://www.localeplanet.com/java/en-PH/index.html


	Section IV: Environment Variables
		> Environment variables are important for hiding sensitive pieces of information like the backend API URL which can be exploited if added directly into our code
*/