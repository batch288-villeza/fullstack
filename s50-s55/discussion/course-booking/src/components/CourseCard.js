import {Container, Row, Col, Card, Button} from 'react-bootstrap';

//import the useState hook from react
import {useState, useEffect} from 'react';

//
import {useContext} from 'react';
import UserContext from '../UserContext.js'
import { Link } from 'react-router-dom';


// import sweetalert2
import Swal2 from 'sweetalert2';

export default function CourseCard(props){

    

    const {user} = useContext(UserContext);

    // console.log(props.courseProp[0].id)
    // console.log(props.courseProp)

    // Array/Object Destructuring
    const {_id, name, description, price} = props.courseProp
    // console.log(id)
    // console.log(props.courseProp)

    function phpCurrency(value){
        let phpCurrency = Intl.NumberFormat('en-PH',
            {
                style:'currency',
                currency:'PHP'
                // useGrouping:true,
                // maximumSignificantDigits:3
            }
        );
    
        return phpCurrency.format(value);
    }

    // Use the state hook for this component to be able to store its state
    // States are use to keep track of information related to individual components
    //      Syntax:
    //          const [getter, setter] = useState(initialGeterValue)
    // you can't just normally re-assign the value of the state of count.

    const [count, setCount] = useState(0);
    const [seat, setSeat] = useState(30);
    const [isDisable, setIsDisable] = useState(false)
    
    // console.log(count)
    // console.log(seatCount)
    
    // you can't just normally re-assign the value of the state of count.
    // setCount(2)
    // console.log(count)

    // this function will be invoked when the button is clicked
    function enroll(){
        if(seat !== 0){
            setCount(count+1);
            setSeat(seat-1);
        }else{
            setIsDisable(true);
            // alert("No more seats!");



        }
    }

    function login(){
        
    }

    // the function or the side effext in our useEffect hok will be invoked or run on the initial loading of our application and when there is/are change/changes on our dependencies.
    /*
        Syntax:
            useEffect(<function>, <dependency>)

        > you can use multiple dependency
        > you can also use a blank dependency
    */
    useEffect(()=>
        {
            if(seat === 0){
                setIsDisable(true)
                Swal2.fire({title: 'Courses', icon: 'warning', text:'Sorry, therer no more seats available!'})
            }
        },
        [seat]
    );

    return(
        <Container>
            <Row>
                <Col className="col-12 gy-3">
                    <Card className="cardHighlight">
                        <Card.Body>
                            <Card.Title>{name}</Card.Title>
                            <Card.Subtitle>Description:</Card.Subtitle>
                            <Card.Text>{description}</Card.Text>

                            <Card.Subtitle>Price:</Card.Subtitle>
                            <Card.Text>{phpCurrency(price)}</Card.Text>

                            <Card.Subtitle>Enrollees:</Card.Subtitle>
                            <Card.Text>{count}</Card.Text>

                            {
                                user !== null ?
                                    <Button as={Link} to={`/courses/${_id}`}>Details</Button>
                                :
                                    <Button as={Link} to='/login'>Login to Enroll</Button>
                            }
                        </Card.Body>
                    </Card>
                </Col>
            </Row>
        </Container>
    );
}