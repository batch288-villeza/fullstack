import React from "react";


// Create a Context Object

// A context object as the name states is a data that can be used to store information that can be shared to other components within the app.

// The context object is a different approach to passing information between components and allows easier access by avoiding the use of prop passing.

// with the help of the .createContext() method, we were able to create a context stored in a variable UserContext.
const UserContext = React.createContext();


// The "Provider" component allows other component to consume/use the context object and supply necessary information needed to the context object.
export const UserProvider = UserContext.Provider;

export default UserContext;