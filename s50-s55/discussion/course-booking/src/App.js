import 'bootstrap/dist/css/bootstrap.min.css';

import './App.css';

//
import {useState, useEffect} from 'react';

// import UserProvider
import { UserProvider } from './UserContext.js';


import AppNavBar from './components/AppNavBar.js';

import Courses from './pages/Courses.js';
import CourseView from './pages/CourseView.js'
import Home from './pages/Home.js';
import Login from './pages/Login.js';
import Logout from './pages/Logout.js'
import PageNotFound from './pages/PageNotFound';
import Register from './pages/Registration.js';

// BrowserRouter - is a component that will enable us to simulate page navigation by synchronizing the shown content and the shown URL in the web browser.
// Routes - this component holds all our Route components. It select which 'Route' component to show based on the url endpoint

import {BrowserRouter, Route, Routes} from 'react-router-dom';

function App() {

  // const [user, setUser] = useState(localStorage.getItem('email'));
  const [user, setUser] = useState({id: null, isAdmin: null});
  // const [user, setUser] = useState(localStorage.getItem('token'));


  const unsetUser = () =>{
    localStorage.clear();
  }


  // to check if there are data captured from database
  useEffect(()=>{
      // console.log('This is App.js')
      // console.log(user)
      // console.log(localStorage.getItem('token'))

    },
    [user]
  )

  //
  useEffect(()=>{
      if(localStorage.getItem('token')){
        fetch(`${process.env.REACT_APP_API_URL}/users/userdetails`,
            {
                method: 'GET',
                headers: {
                    Authorization: `Bearer ${localStorage.getItem('token')}`
                }
            }
        )
        .then(result => result.json())
        .then(data => {
            // console.log(data)
            setUser({
                    id: data._id,
                    isAdmin: data.isAdmin
                })
        // console.log('This is retrieveUserDetails in Login.js')
        // console.log(user);
        });
      }
    },
    []
  )  

  return (
    <UserProvider value={{user, setUser, unsetUser}}>
      <BrowserRouter>
        <AppNavBar/>
        <Routes>
          <Route path='/'           element={<Home/>}/>
          <Route path='/courses'    element={<Courses/>}/>
          <Route path='/courses/:courseId' element={<CourseView/>}/>
          <Route path='/register'   element={<Register/>}/>
          <Route path='/login'      element={<Login/>}/>
          <Route path='/logout'     element={<Logout/>}/>
          <Route path='*'           element={<PageNotFound/>}/>
        </Routes>
      </BrowserRouter>
    </UserProvider>
  );
}

export default App;
