import {Container, Row, Col, Button, Form, Alert} from 'react-bootstrap';
import {useState ,useEffect, useContext} from 'react';

//
import UserContext from '../UserContext.js';

//
import {Navigate, Link, useNavigate} from 'react-router-dom';


// import sweetalert2
import Swal2 from 'sweetalert2';


export default function Login(){

    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [isDisble, setIsDisable] = useState(true);

    // localStorage.getItem(key) - this gets the value of the specified key
    // const [user, setUser] = useState(localStorage.getItem('email'));
    
    // we are going to consume/use the UserContext we created from use UserContext.js
    const {user, setUser} = useContext(UserContext)

    const navigateTo = useNavigate();


    const retrieveUserDetails = (token)  => {
        fetch(`${process.env.REACT_APP_API_URL}/users/userdetails`,
            {
                method: 'GET',
                headers: {
                    Authorization: `Bearer ${token}`
                }
            }
        )
        .then(result => result.json())
        .then(data => {
            // console.log(data)
            setUser({
                    id: data._id,
                    isAdmin: data.isAdmin
                })
        console.log('This is retrieveUserDetails in Login.js')
        console.log(user);
        });
    }

    useEffect(()=>
        { 
            if(email !== '' && password !== ''){
                setIsDisable(false);
            }else{
                setIsDisable(true);
            }
        },
        [email, password]
    );

    const login = (event) => {
        // to prevent page from loading once an action has been made
        event.preventDefault();

        fetch(`${process.env.REACT_APP_API_URL}/users/login`,
            {
                method: 'POST',
                headers:{
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(
                    {
                        email: email,
                        password: password
                    }
                )
            }
        )
        .then(result => result.json())
        .then(data => {
            if(data === false){
                Swal2.fire({
                    title: 'Authentication Failed!',
                    icon: 'error',
                    text: 'Check your login details and try again!'
                })
            }else{
                localStorage.setItem('token', data.auth);
                retrieveUserDetails(data.auth)
                
                Swal2.fire({
                    title: 'Login Sucessful',
                    icon: 'success',
                    text: 'Welcome to Zuitt!'
                })

                navigateTo('/');
            }
        });
/*
        // set the email of the authenticated user ib the local storage
        // Syntax:
        //      localStorage.setItem('propertyName', value)
        // localStorage.setItem('email', email);
        // setUser(localStorage.getItem('email'));
        
        

        // alert("You are now logged in!");
        // console.log(email);

        // // set 
        // setEmail('');
        // setPassword('');
        // setIsDisable(true);

        // we are going to process a fetch request to the corresponding backend API
        //  Syntax:
        //      fetch(url, {options});
*/        
    }

    // console.log('This is before return in Login.js')
    // console.log(user);
    // this usees a ternary operator
    return(
        (user.id === null) ?
            <Container>
                <Row>
                    <Col className='col-12 col-md-6 mx-auto mt-5 p-3 border border-1 border-light shadow-sm'>
                        <h1>Login</h1>
                        <div className='mt-3'>
                            <Form onSubmit={event => login(event)}>
                                <Form.Group className="mb-3" controlId="email">
                                    <Form.Label>Email address</Form.Label>
                                    <Form.Control
                                        type="email"
                                        placeholder="Enter email"
                                        value={email}
                                        onChange={
                                            event => {
                                                setEmail(event.target.value)
                                            }
                                        }
                                    />
                                </Form.Group>

                                <Form.Group className="mb-3" controlId="formBasicPassword1">
                                    <Form.Label>Password</Form.Label>
                                    <Form.Control
                                        type="password"
                                        placeholder="Password" 
                                        value={password}
                                        onChange={
                                            event => {
                                                setPassword(event.target.value)
                                            }
                                        }
                                    />
                                </Form.Group>

                                <p>No account yet? <Link to="/register">Sign up here</Link></p>

                                <Button variant="success" type="submit" disabled={isDisble}>
                                    Login
                                </Button>
                            </Form>
                        </div>
                    </Col>
                </Row>
            </Container>
        : <Navigate to='/accessDenied'/>
    )
}