import {useState, useEffect} from 'react';
import {Container, Row, Col, Button, Card} from 'react-bootstrap';
import {useParams, useNavigate} from 'react-router-dom';


import Swal2 from 'sweetalert2';

export default function CourseView(){

	const [name, setName] = useState('');
	const [description, setDescription] = useState('');
	const [price, setPirce] = useState('');

	const {courseId} = useParams();
	// console.log(courseId);

	const navigateTo = useNavigate();

	useEffect(() => {
		fetch(`${process.env.REACT_APP_API_URL}/courses/${courseId}`)
		.then(result => result.json())
		.then(data =>{
				// console.log(data)

				setName(data.name);
				setDescription(data.description);
				setPirce(data.price);
			}
		)

	}, []);

	const enroll = (courseId) => {
		// we are going to fetch the route
		fetch(`${process.env.REACT_APP_API_URL}/users/enroll`,
			{
				method: 'POST',
				headers: {
					'Authorization': `Bearer ${localStorage.getItem('token')}`,
					'Content-Type': 'application/json'
				},
				body: JSON.stringify(
					{
						id: courseId
					}
				)

			}
		)
		.then(response => response.json())
		.then(result => {
			console.log(result)
			if(result){
				Swal2.fire({
					title: 'Successfully enrolled!',
					icon: 'success',
					text: 'You have successfully enrolled for this course!'
				})
				navigateTo('/courses');
			}else{
				Swal2.fire({
					title: 'Something went wrong!',
					icon: 'error',
					text: 'Please try agian!'
				})
			}
		})
	}

	return(
		<Container>
			<Row>
				<Col>
					<Card>
				      	<Card.Body>
					        <Card.Title>{name}</Card.Title>
					        
					        <Card.Subtitle>Description</Card.Subtitle>
					        <Card.Text>{description}</Card.Text>

					        <Card.Subtitle>Price</Card.Subtitle>
					        <Card.Text>Php {price}</Card.Text>

					        <Card.Subtitle>Class Schedule</Card.Subtitle>
					        <Card.Text>8:00AM - 5:00PM</Card.Text>

					        <Button variant="primary" onClick={()=>enroll(courseId)}>Enroll</Button>
				      	</Card.Body>
				    </Card>
				</Col>
			</Row>
		</Container>
	)
}