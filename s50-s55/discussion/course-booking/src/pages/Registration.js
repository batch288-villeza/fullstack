import {Container, Row, Col, Button, Form} from 'react-bootstrap';
import {useState, useEffect, useContext} from 'react';
import {Link, useNavigate} from 'react-router-dom';
import UserContext from '../UserContext.js';
import Swal2 from 'sweetalert2';


export default function Register(){
    const navigateTo = useNavigate();
    const {user} = useContext(UserContext);
    
    const [firstName, setFirstName] = useState('');
    const [lastName, setLastName] = useState('');
    const [mobileNo, setMobileNo] = useState('');
    const [email, setEmail] = useState('');
    const [password1, setPassword1] = useState('');
    const [password2, setPassword2] = useState('');

    const [isDisble, setIsDisable] = useState(true);


    const [message, setMessage] = useState('');
    const [mobileNoMessage, setMobileNoMessage] = useState('');
    const [emailMessage, setEmailMessage] = useState('');
    const [passwordMessage, setPasswordMessage] = useState('');
    const [retypePasswordMessage, setRetypePasswordMessage] = useState('');


    const [isMobileNo       , setIsMobileNo         ] = useState(false);
    const [isEmail          , setIsEmail            ] = useState(false);
    const [isPassword       , setIsPassword         ] = useState(false);
    const [isRetypePassword , setIsRetypePassword   ] = useState(false);
    const [isMessage        , setIsMessage          ] = useState(false);

    function onlyNumberKey(evt) {
        // Only ASCII character in that range allowed
        let ASCIICode = (evt.which) ? evt.which : evt.keyCode
        if(ASCIICode > 31 && (ASCIICode < 48 || ASCIICode > 57))
            return false;
        return true;
    }

    const checkEmail= (getEmail) => {
        fetch(`${process.env.REACT_APP_API_URL}/users/verifyEmail`,
            {   
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({
                    email: getEmail
                })
            }
        )
        .then(response => response.json())
        .then(result => {
            if(result){
                setEmailMessage('Email already exist!');
            }else{
                setEmailMessage('');            }
        })
    }

    useEffect(() =>
        {   
            if(mobileNo.length >= 11){
                setMobileNoMessage('')
            }else{
                setMobileNoMessage('Mobile number must be 11 digits')
            }
        },
        [mobileNo]
    );

    useEffect(() =>
        {   
            if(!email){
                setEmailMessage('')
            }else{
                checkEmail(email)
            }
        },
        [email]
    )

    useEffect(() =>
        {
            if(!password1){
                setPasswordMessage('')
            }else{
                if(password1.length < 7){
                    setPasswordMessage('Your password length must be greater than or equal to 7!')
                }else{
                    setPasswordMessage('')
                }
            }

            if(!password2){
                setRetypePasswordMessage('')
            }else{
                if(password2 !== password1){
                    setRetypePasswordMessage('Password did not match')
                }else{
                    setRetypePasswordMessage('')
                }
            }   
        },
        [password1, password2]
    );


    useEffect(() =>
        {      

            if(firstName !== null && lastName !== null && email !== null && password1 !== null && password2 !== null && password1===password2 && emailMessage === ''){
                setMessage('')
                setIsDisable(false)
            }else{
                setMessage('All required fields must be provide')
                setIsDisable(true)
            }
        },
        [
            firstName, lastName, mobileNo, email, password1, password2
        ]
    )

    function register(e){
        // prevents page to  reload
        e.preventDefault();

        fetch(
            `${process.env.REACT_APP_API_URL}/users/registration`,
            {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({
                    firstName: firstName,
                    lastName: lastName,
                    mobileNo: mobileNo,
                    email: email,
                    password: password1
                })
            }
        )
        .then(response => response.json())
        .then(result => {
            if(result){
                Swal2.fire({
                    title: 'Successfully Registered!',
                    icon: 'success',
                    text: ''
                })
                navigateTo('/login')
            }else{
                Swal2.fire({
                    title: 'Failed Registered!',
                    icon: 'error',
                    text: 'Something went wrong, please try agian!'
                })
            }
        })
    }

    return(
        (user.id === null) ?
            <Container className='mt-5'>
                <Row>
                    <Col className="col-12 col-md-6 mx-auto mt-5 p-3 border border-1 border-light shadow-sm">
                        <h1 className='text-center'>Register</h1>
                        <div className='mt-3'>
                            <Form onSubmit={event => register(event)}>

                                <Form.Group className="mb-3" controlId="firstName">
                                    <Form.Label>First Name</Form.Label>
                                    <Form.Control
                                        type="text"
                                        placeholder="First Name"
                                        value={firstName}
                                        onChange={
                                            event => {
                                                setFirstName(event.target.value)
                                            }
                                        }
                                        required
                                    />
                                </Form.Group>

                                <Form.Group className="mb-3" controlId="lastName">
                                    <Form.Label>Last Name</Form.Label>
                                    <Form.Control
                                        type="text"
                                        placeholder="Last Name"
                                        value={lastName}
                                        onChange={
                                            event => {
                                                setLastName(event.target.value)
                                                // console.log(email)
                                            }
                                        }
                                        required
                                    />
                                </Form.Group>

                                <Form.Group className="mb-3" controlId="mobileNo">
                                    <Form.Label>Mobile No</Form.Label>
                                    <Form.Control
                                        type="text"
                                        placeholder="Mobile Number"
                                        maxLength="11"
                                        value={mobileNo}
                                        onKeyPress={(event) => {
                                            if (!/[0-9]/.test(event.key)) {
                                              event.preventDefault();
                                            }
                                          }
                                        }
                                        onChange={
                                            event => {
                                                setMobileNo(event.target.value)
                                                // console.log(email)
                                            }
                                        }
                                        required
                                    />
                                    <p className="text-danger text-center">{mobileNoMessage}</p>
                                </Form.Group>

                                
                            
                                {/*Email*/}
                                <Form.Group className="mb-3" controlId="email">
                                    <Form.Label>Email address</Form.Label>
                                    <Form.Control
                                        type="email"
                                        value={email}
                                        onChange={
                                            event => {
                                                setEmail(event.target.value)
                                                // console.log(email)
                                            }
                                        }
                                        placeholder="Enter email"
                                        required
                                    />
                                    <p className="text-danger text-center fs-6">{emailMessage}</p>
                                </Form.Group>

                                

                                {/*Password*/}
                                <Form.Group className="mb-3" controlId="password1">
                                    <Form.Label>Password</Form.Label>
                                    <Form.Control
                                        type="password"
                                        placeholder="Password"
                                        value={password1}
                                        onChange={
                                            event => {
                                                setPassword1(event.target.value)
                                                // console.log(password2)
                                            }
                                        }
                                        required
                                    />
                                    <p className="text-danger text-center">{passwordMessage}</p>
                                </Form.Group>

                                {/*Retype Password*/}
                                <Form.Group className="mb-3" controlId="password2">
                                    <Form.Label>Confirm Password</Form.Label>
                                    <Form.Control
                                        type="password"
                                        placeholder="Retype your nominated password"
                                        value={password2}
                                        onChange={
                                            event => {
                                                setPassword2(event.target.value)
                                                // console.log(password2)
                                            }
                                        }
                                        required
                                    />
                                    <p className="text-danger text-center">{retypePasswordMessage}</p>
                                </Form.Group>

                                <p className="text-danger text-center">{message}</p>

                                <p>Have an account? <Link to='/login'>Login here</Link></p>

                                <Button
                                    variant="primary"
                                    type="submit"
                                    disabled={isDisble}
                                >
                                    Submit
                                </Button>
                            </Form>
                        </div>
                    </Col>
                </Row>
            </Container>
        : 
        <Navigate to='/accessDenied'/>
    );
}