import {Container, Row, Col} from 'react-bootstrap';
import {Link} from 'react-router-dom';

export default function PageNotFound(){
    return(
        <Container>
            <Row>
                <Col>
                    <h1>Page Not Found</h1>
                    <p>Go back to <Link to="/">homepage</Link>!</p>
                </Col>
            </Row>
        </Container>
    )
}