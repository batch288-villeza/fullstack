import coursesData from '../data/courses.js';
import CourseCard from '../components/CourseCard.js';

import {useState, useEffect} from 'react';

export default function Courses(){

    // we console the coursesData to check if it is working poperly
    // console.log(coursesData);
    
    // the courseProp taht will be passed in an object 


    const [courses, setCourses] = useState([]);
    useEffect(()=>{
            fetch(`${process.env.REACT_APP_API_URL}/courses/activeCourses`)
            .then(result => result.json())
            .then(data => {
                // console.log(data)
                setCourses(data.map(course =>{
                    return(
                        <CourseCard key={course._id} courseProp={course}/>
                    )
                }))
            })
        },[]
    );

    // const courses = coursesData.map(course =>{
    //     return(
    //         <CourseCard key={course.id} courseProp={course}/>
    //     )
    // });

    return(
        <>
            <h1 className='text-center mt-3'>Courses</h1>
            {courses}
        </>
    )
}