// import {Navigate, useNavigate} from "react-router-dom";
import {Navigate, useNavigate} from "react-router-dom";
import {useEffect, useContext} from "react";
import UserContext from "../UserContext";

export default function Lagout(){

    // to clear the content of the local storage we have to use the clear() method
    // localStorage.clear();

    const navigateTo = useNavigate()

    // Using Context
    const {setUser, unsetUser} = useContext(UserContext);

    useEffect(()=>{
        unsetUser();

        // setUser(localStorage.getItem('email'))
        setUser({
            id:null,
            isAdmin:null
        })
    });

    return(
        <Navigate to='/login'/>
    )

}