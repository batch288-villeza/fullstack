import React from 'react';
import ReactDOM from 'react-dom/client';

import App from './App';

// createdRoot() - assigns the element to be managed by react with its virtual dom
const root = ReactDOM.createRoot(document.getElementById('root'));


// render() - displaying the component/react in to the root
root.render(

  // mount App.js
  <React.StrictMode>
    <App />
  </React.StrictMode>
);


// Example: 
// const name = 'John Smith';
// const element = <h1>Hello, {name}!</h1>;

// root.render(element);