// console.log("Hello")

/*	Section I: Document Manipulation Object Model (DOM)

		> allows us to access or modify the properties of an html element in a webpage
		> it is standard onhow to et, change, add or delete HTML elements
		> we will be focussing only with DOM in terms of managing forms
*/


/*
		a. querySelector()
			> it can access/get the HTML elements
			> Uses CSS Selector
				1. id selector (#)
				2. class selector (.)
				3. tag/type selector (html tags)
				4. universal selector (*)
				5. attribute selector ([attribute])
*/

let firstElement = document.querySelector('#txt-first-name');
console.log(firstElement);

/*
			> Two Types Query Selector:
				1. querySelector()
				2. querySelectorAll()
*/

//	1. querySelector()
let secondElement = document.querySelector('.full-name');
console.log(secondElement);

//	2. querySelectorAll()
let thridElement = document.querySelectorAll('.full-name');
console.log(thridElement);


// 		> besides from query selectors, we can also use the "get Element Selectors"
let elementById = document.getElementById('fullName')
console.log(elementById)

let elementByClassName = document.getElementsByClassName('full-name')
console.log(elementByClassName)


/*
	Section 2: Event Listeners
		> whenever a user interacts with a webpage, this action is considered as an event.
		> Working with events is a large part of creating interactivity in a web page
		> specific function will be invoke when an event happens

		> Common Event Listener
			- change
				an element has been chnaged (e.g. input value).
			- click
				and element has been clicked (e.g. button clicks)
			- load
				when the browser has finished loading a webpage.
			- keydown
				when a keyboard key was press down
			- keyup
				when keyboard key was released after being pushed down
*/

/*
		a. addEventListener() function
			- takes two arguments
			- 1st argument is a string identifying the event
			- 2nd argument is a function that the listener will invoke once the specific event occurs.
*/

secondElement = document.querySelector('#txt-last-name');

let fullname = document.querySelector('#fullName');
console.log(fullname)



firstElement.addEventListener('keyup', () => {
	console.log(firstElement.value);
	fullname.innerHTML = `${firstElement.value} ${secondElement.value}`;

});

secondElement.addEventListener('keyup', () => {
	console.log(secondElement.value)
	fullname.innerHTML = `${firstElement.value} ${secondElement.value}`;
});


/*	Reference for value vs innerHTML:
	https://superuser.com/questions/1037389/innerhtml-vs-value-when-and-why#:~:text=value%20refers%20to%20an%20attribute,a%20tag's%20beginning%20and%20end.
*/



// Activity
let changeColor = document.querySelector('#text-color');

changeColor.addEventListener("click", () => {
	switch(changeColor.value){
		case "black":
			fullname.style.color = "black"
			break;
		case "red":
			fullname.style.color = "red"
			fullname
			break;
		case "green":
			fullname.style.color = "green"
			break;
		case "blue":
			fullname.style.color = "blue"
			break;
	}
})

changeColor.addEventListener("keyup", () => {
	switch(changeColor.value){
		case "black":
			fullname.style.color = "black"
			break;
		case "red":
			fullname.style.color = "red"
			fullname
			break;
		case "green":
			fullname.style.color = "green"
			break;
		case "blue":
			fullname.style.color = "blue"
			break;
	}
})